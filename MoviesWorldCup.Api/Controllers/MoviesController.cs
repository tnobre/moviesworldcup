﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MoviesWorldCup.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MoviesController : ControllerBase
    {

        private readonly ILogger<MoviesController> _logger;
        
        private MoviesBusiness moviesBusiness { get; set; }

        public MoviesController(ILogger<MoviesController> logger, MoviesBusiness moviesBusiness)
        {
            _logger = logger;
            this.moviesBusiness = moviesBusiness;
        }

        [HttpGet]
        public IEnumerable<Movie> Get()
        {
            return moviesBusiness.List();
        }

        [HttpPost]
        [Route("championship")]
        public MovieChampionShipResult championship(IEnumerable<Movie> movies)
        {
            return moviesBusiness.RunChampionship(movies);
        }
    }
}
