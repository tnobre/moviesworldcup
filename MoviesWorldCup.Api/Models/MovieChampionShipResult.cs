using System;
using System.Text.Json.Serialization;
public class MovieChampionShipResult {

    public Movie First { get; set; }

    public Movie Second { get; set; }

    public MovieChampionShipResult() {
        
    }

    public MovieChampionShipResult(Movie first, Movie second) {
        this.First = first;
        this.Second = second;
    }

}