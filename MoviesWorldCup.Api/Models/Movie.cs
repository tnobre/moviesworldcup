using System;
using System.Text.Json.Serialization;
public class Movie {


    [JsonPropertyName("id")]
    public string ID { get; set; }

    [JsonPropertyName("titulo")]
    public string Title { get; set; }

    [JsonPropertyName("ano")]
    public int Year { get; set; }

    [JsonPropertyName("nota")]
    public decimal Rating { get; set; }

    public override string ToString() {
        return String.Format("ID: {0}, Title: {1}, Year: {2}, Rating: {3}", ID, Title, Year, Rating);
    }

}