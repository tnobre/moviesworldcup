using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

public class MoviesRepository : IRepository<Movie>
{

    private IConfiguration Configuration { get; set; }

    private IDictionary<string, Movie> _Movies { get; set; }

    public MoviesRepository(IConfiguration configuration)
    {
        this.Configuration = configuration;
        this.LoadMovies();
    }

    private void LoadMovies()
    {
        if (this._Movies == null)
        {
            var config = this.Configuration.GetSection("MoviesWordCupExternalAPI");

            this._Movies = new Dictionary<string, Movie>();

            using (var client = new HttpClient())
            {
                var jsonString = client.GetStringAsync(String.Concat(config["URL"], config["Resources:GetMovies"]));

                jsonString.Wait();

                var parsed = JsonSerializer.Deserialize<IEnumerable<Movie>>(jsonString.Result);

                foreach (var item in parsed)
                {
                    this._Movies[item.ID] = item;
                    // Console.WriteLine(item);
                }
            }
        }
    }

    public Movie GetById(string id)
    {
        return this._Movies[id];
    }

    public IEnumerable<Movie> List()
    {
        return this._Movies.Select(p => p.Value);
    }

    public void Save(Movie movie)
    {
        throw new NotImplementedException();
    }

    public void Delete(Movie movie)
    {
        throw new NotImplementedException();
    }

}