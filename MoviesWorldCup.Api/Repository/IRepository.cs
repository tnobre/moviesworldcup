using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

public interface IRepository<T>
{
    
    T GetById(string id);
    
    IEnumerable<T> List();

    void Save(T model);

    void Delete(T model);

}