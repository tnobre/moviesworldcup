using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

public class MoviesBusiness
{

    private IRepository<Movie> repository { get; set; }

    public MoviesBusiness(IRepository<Movie> repository)
    {
        this.repository = repository;
    }

    public IEnumerable<Movie> List()
    {
        return this.repository.List();
    }

    public MovieChampionShipResult RunChampionship(IEnumerable<Movie> movies)
    {
        var matches = new Queue<Movie>();
        Movie a = null, b = null;

        {
            var list = new List<Movie>();

            foreach (var movie in movies)
            {
                var m = this.repository.GetById(movie.ID);
                if (m == null)
                {
                    m = movie;
                }
                list.Add(m);
            }

            if (movies == null || list.Count < 2)
            {
                throw new ArgumentException("you must pass 2 movies at least");
            }
            else if (!this.IsCountAPowerOf2(list.Count))
            {
                throw new ArgumentException("movies count must be power of 2");
            }

            a = list[0];
            b = list[list.Count - 1];
            matches.Enqueue(WhoWinsBetween(a, b));

            for (int i = 1, j = list.Count, k = j / 2; i < k; i++)
            {
                var c = list[i];
                var d = list[j - i - 1];

                // play the first phase
                matches.Enqueue(WhoWinsBetween(c, d));
            }
        }


        while (matches.Count >= 2)
        {
            a = matches.Dequeue();
            b = matches.Dequeue();

            matches.Enqueue(WhoWinsBetween(a, b));
        }

        var first = matches.Dequeue();
        var second = a == first ? b : a;

        return new MovieChampionShipResult(first, second);
    }

    private Movie WhoWinsBetween(Movie a, Movie b)
    {
        if (a.Rating > b.Rating)
        {
            return a;
        }
        else if (b.Rating > a.Rating)
        {
            return b;
        }
        else
        {
            int comparison = String.Compare(a.Title, b.Title, comparisonType: StringComparison.OrdinalIgnoreCase);

            if (comparison <= 0)
            {
                return a;
            }
            else
            {
                return b;
            }
        }
    }

    private bool IsCountAPowerOf2(int n)
    {
        return Math.Log2(n) == Math.Ceiling(Math.Log2(n));
    }

}