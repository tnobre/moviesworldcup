import React from "react";

export default function MoviesResultItem({ movie, position }) {

    return (
        <li>
            <label>
            <b>{position}&ordm; {movie.titulo}</b><br/>
            <b>Nota: </b><span>{movie.nota}</span>
            </label>
        </li>
    );
};