import React from "react";

export default function MoviesListItem({ movie, onClick, onChange }) {

    return (
        <li>
            <label>
                <input type="checkbox" name={movie.id} onClick={onClick} />
                <b>{movie.titulo}</b><br/>
                {movie.nota}
            </label>
        </li>
    );
};