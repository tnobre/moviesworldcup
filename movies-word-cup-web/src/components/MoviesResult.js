import React, { useState, useEffect } from "react";
import MoviesResultItem from './MoviesResultItem';
import { Link } from "react-router-dom";
import "./styles/MovieResult.css";

export default function MoviesResult({ moviesSelected, checkedMaximum }) {

    const [isLoaded, setIsLoaded] = useState(false);
    const [error, setError] = useState(null);
    const [movies, setMovies] = useState([]);
    const championshipEndpoint = 'https://localhost:5001/movies/championship';

    // console.log(moviesSelected);

    useEffect(() => {
        if (moviesSelected && moviesSelected.length === checkedMaximum) {
            fetch(championshipEndpoint, {
                method: "POST",
                body: JSON.stringify(moviesSelected),
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                }
            })
                .then(res => res.json())
                .then(
                    (result) => {
                        setMovies([result.first, result.second]);
                        setIsLoaded(true);
                    },
                    (error) => {
                        setError(error);
                        setIsLoaded(true);
                    }
                )
        } else {
            setError(new Error("Incorret movies number"));
            setIsLoaded(true);
        }
    }, [moviesSelected, checkedMaximum])

    if (!isLoaded) {
        return "Aguarde o cálculo do resultado";
    } else if (!!error) {
        return `Erro ao calcular o resultado: ${error}`;
    } else {
        return (
            <div className="movie-result">
                <div className="movie-result-head">
                    <Link to={{
                        pathname: "/"
                    }} className="btn movie-selector-btn">Novo campeonato</Link>
                </div>
                <div className="movie-result-body">
                    <ul>
                        {movies.map((movie, i) => {
                            return <MoviesResultItem key={movie.id} movie={movie} position={i + 1} />
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}
;