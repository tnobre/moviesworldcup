import React, { useState, useEffect } from "react";
import MoviesListItem from './MoviesListItem';
import { Link } from "react-router-dom";
import "./styles/MovieSelector.css";

export default function MoviesList() {
    const CHECKED_MAXIMUM = 8;
    const [isLoaded, setIsLoaded] = useState(false);
    const [error, setError] = useState(null);
    const [movies, setMovies] = useState([]);
    const [checkedCount, setCheckedCount] = useState(0);
    const [checkedMovies] = useState({});
    const moviesEndpoint = 'https://localhost:5001/movies';

    const handleOnClick = (e) => {
        var checked = e.target.checked;
        if (checked && checkedCount >= CHECKED_MAXIMUM) {
            e.preventDefault();
            return false;
        } else {
            var id = e.target.name;
            if (checked) {
                checkedMovies[id] = { id };
                setCheckedCount(checkedCount + 1);
            } else {
                delete checkedMovies[id];
                setCheckedCount(checkedCount - 1);
            }
        }
    }

    useEffect(() => {
        fetch(moviesEndpoint)
            .then(res => res.json())
            .then(
                (result) => {
                    setMovies(result);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                }
            )
    }, [])

    if (!isLoaded) {
        return "Aguarde o carregamento da lista dos filmes";
    } else if (!!error) {
        return `Erro ao carregar a lista de filmes: ${error}`;
    } else {
        console.log(checkedCount, CHECKED_MAXIMUM, checkedCount === CHECKED_MAXIMUM);
        return (
            <div className="movie-selector">
                <div className="movie-selector-head">
                    <div className="movie-selector-counter">
                        <div>
                            Selecionados<br />
                            {checkedCount} de {CHECKED_MAXIMUM} filmes
                        </div>
                    </div>
                    <div className="movie-selector-generate-button">
                        { checkedCount === CHECKED_MAXIMUM && <Link to={{
                            pathname: "/result",
                            state: { movies: Object.values(checkedMovies), checkedMaximum: CHECKED_MAXIMUM }
                        }} className="btn movie-selector-btn">
                            Gerar meu campeonato
                        </Link>}
                    </div>
                </div>
                <div className="movie-selector-body">
                    <ul>
                        {movies.map((movie) => {
                            return <MoviesListItem key={movie.id} movie={movie} onClick={handleOnClick} />
                        })}
                    </ul>
                </div>
            </div>
        );
    }
};
