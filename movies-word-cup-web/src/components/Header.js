import React, { useEffect } from "react";
import { Link } from "react-router-dom";

export default function Header({title, subtitle}) {

    useEffect(() => {
        document.title = `${title} - Campeonato de filmes`;
    })

    return (
        <header className="main-header">
            <div className="main-header-bg">
                <div className="wrapper">
                    <div className="main-header-content">
                        <h1><Link to="/">CAMPEONATO DE FILMES</Link></h1>
                        <h2>{title}</h2>
                        <h3>{subtitle}</h3>
                    </div>
                </div>
            </div>
        </header>
    );
}
;