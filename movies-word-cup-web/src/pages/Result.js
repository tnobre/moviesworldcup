import React from "react";
import Header from "../components/Header"
import MoviesResult from "../components/MoviesResult"
import {
    Route,
    Redirect,
    useLocation,
  } from "react-router-dom";

export default function Result() {

    const { state } = useLocation();

    if (!state) {
        return (
            <Route>
                <Redirect to="/" />
            </Route>
        );
    } else {
        const { movies, checkedMaximum } = state;

        return (
            <div>
                <Header 
                    title="Resultado Final" 
                    subtitle="Veja o resultado final do Campeonato de 
                    filmes de forma simples e rápida" />
                <main className="main-content">
                    <div className="wrapper">
                        <MoviesResult moviesSelected={movies} checkedMaximum={checkedMaximum} />
                    </div>
                </main>
            </div>
        );
    }
};