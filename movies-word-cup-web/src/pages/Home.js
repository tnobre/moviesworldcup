import React from "react";
import Header from "../components/Header"
import MovieSelector from "../components/MoviesSelector"

export default function Home() {
    return (
        <div>
            <Header 
                title="Fase de seleção" 
                subtitle="Selecione 8 filmes que você deseja que entrem na competição e 
                depois pressione o botão Gerar Meu Campeonato para prosseguir" />
            <main className="main-content">
                <div className="wrapper">
                    <MovieSelector />
                </div>
            </main>
        </div>
    );
}