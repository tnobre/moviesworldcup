using System;
using Xunit;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.IO;

namespace MoviesWorldCup.Tests
{

    public class MoviesTest
    {

        [Fact]
        public async void ChampionshipTest()
        {
            using (var httpClientHandler = new HttpClientHandler())
            {
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var filestream = new FileStream("movies.championshiptest.json", FileMode.Open);
                    var request = new StreamContent(filestream);

                    request.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    HttpResponseMessage response = await client.PostAsync("https://localhost:5001/movies/championship", request);
                    response.EnsureSuccessStatusCode();

                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();

                    var result = JsonSerializer.Deserialize<MovieChampionShipResult>(jsonString.Result, new JsonSerializerOptions
                    {
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    });

                    Assert.True(result.First != null);
                    Assert.True(result.Second != null);

                    Assert.True(result.First.Title == "Vingadores: Guerra Infinita");
                    Assert.True(result.Second.Title == "Os Incríveis 2");
                }
            }
        }

        [Fact]
        public async void ChampionshipDrawTest()
        {
            using (var httpClientHandler = new HttpClientHandler())
            {
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var filestream = new FileStream("movies.championshiptest.draw.json", FileMode.Open);
                    var request = new StreamContent(filestream);

                    request.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    HttpResponseMessage response = await client.PostAsync("https://localhost:5001/movies/championship", request);
                    response.EnsureSuccessStatusCode();

                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();

                    var result = JsonSerializer.Deserialize<MovieChampionShipResult>(jsonString.Result, new JsonSerializerOptions
                    {
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    });

                    Assert.True(result.First != null);
                    Assert.True(result.Second != null);

                    Assert.True(result.First.Title == "Hereditário");
                    Assert.True(result.Second.Title == "Upgrade");
                }
            }
        }
    }
}
