# Movies World Cup

Olá, este é o teste da Copa Filmes, uma aplicação SPA que utiliza ReactJS no frontend e .NET Core no backend.

Após o clone, para rodar a aplicação você precisará do *dotnet client* e do *npm*

Para iniciar o backend, execute o comando a seguir no terminal, na pasta **MoviesWordCup.Api**:

    $ dotnet run

Em seguida, acesse https://localhost:5001/movies e para ver a listagem dos filmes retornados na API.



Em outro terminal, vá para a pasta **movies-word-cup-web** e execute os seguintes comandos para iniciar a aplicação em React:

    $ npm install
    
    $ npm start

Aguarde o browser abrir uma janela com o endereço http://localhost:3000/ e a aplicação carregará a lista de filmes da API citada acima.

Os testes podem ser executados na pasta **MoviesWordCup.Tests** executando o comando a seguir:

    $ dotnet test

**Nota:** os testes requerem que a aplicação backend esteja rodando.

## Versões utilizadas

- .NET Core v3.1.3
- NodeJS v12.18.0
- ReactJS v16.13.1
